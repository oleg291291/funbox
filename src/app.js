import './styles/normalize.sass';
import './styles/theme.sass';

const mainPageProducts = document.querySelector('.main-page__products');
mainPageProducts.addEventListener("click", mainPageProductsHandler);
function mainPageProductsHandler(event) {
    var target = event.target;
    if(target.classList.contains('main-page__product') || target.classList.contains('product__tip-to-buy') || target.classList.contains('product__tip-selected')){
        return
    }
    while (target != mainPageProducts) {

        if (target.classList.contains('product__card') && target.parentNode.classList.contains('main-page__product_available') && !target.parentNode.classList.contains('main-page__product_unavailable')) {
            target.classList.add('product__card_hover-disabled');
            target.addEventListener('mouseleave', enableHover)
            function enableHover(event){
                event.target.classList.remove('product__card_hover-disabled');
            }
        }

        if (target.classList.contains('main-page__product') && !target.classList.contains('main-page__product_unavailable')) {
            target.classList.toggle('main-page__product_available');
            target.classList.toggle('main-page__product_selected');
        }
        target = target.parentNode;
    }



}
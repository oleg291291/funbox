const path = require('path');

const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const MiniCssExtractPlugin = require("mini-css-extract-plugin");

const ExtractTextPlugin = require("extract-text-webpack-plugin");

const autoprefixer = require('autoprefixer');

const isDevelopment = process.env.NODE_ENV !== 'production';

console.log('isDevelopment === ' + isDevelopment)

const config = {
    entry: {
        app: './src/app.js'

    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: '[name].[hash].js'
    },
    devtool: isDevelopment && "source-map",
    devServer: {
        port: 3000,
        open: false,
        hot: true,
        contentBase: path.join(__dirname, "src", "html"),
        publicPath: '/',
        historyApiFallback: true,
    },

    module: {
        rules: [
            {
                /////////////
                //for production only! (hot reload not working with this)
                ///////////////
                test: /\.(sass|scss|css)$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    {
                        loader: "css-loader",
                        options: {
                            sourceMap: isDevelopment,
                            minimize: !isDevelopment
                        }
                    },
                    {
                        loader: "postcss-loader",
                        options: {
                            autoprefixer: {
                                browsers: ["last 2 versions"]
                            },
                            sourceMap: isDevelopment,
                            plugins: () => [
                                autoprefixer
                            ]
                        },
                    },
                    {
                        loader: "sass-loader",
                        options: {
                            sourceMap: isDevelopment
                        }
                    }

                ]
            },

            {
                test: /\.(jpe?g|png|gif|svg)$/i,
                use: [
                    'url-loader?limit=10000,name=[name]-[hash].[ext],outputPath=assets/images',
                ]
            },
            {
                test: /\.(woff|woff2|eot|ttf|otf)$/,
                loader: "file-loader",
                options: {
                    name: '[name].[ext]',
                    outputPath: 'assets',
                    useRelativePath: true,
                }

            }

        ]
    },
    plugins: [
        require('autoprefixer'),
        new MiniCssExtractPlugin({
            filename: "[name].[hash].css",
            chunkFilename: "[id].css"
        }),
        new HtmlWebpackPlugin({
            title: 'Funbox-test',
            template: path.resolve(__dirname, 'src', 'html', 'index.html'),
            minify: !isDevelopment && {
                html5: true,
                collapseWhitespace: true,
                caseSensitive: true,
                removeComments: true,
                removeEmptyElements: false
            },

        }),

    ]
};



if (isDevelopment) {
    // for hot reload support in development mode!
    config.module.rules[0] = {
        test: /\.(sass|scss|css)$/,
        use: [
            "style-loader",
            {
                loader: "css-loader",
                options: {
                    module: true
                }
            },
            "sass-loader"
        ]
    }
}


module.exports = config;